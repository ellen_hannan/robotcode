const byte ledPin = 16;
const byte buttonPin = D6;

void setup()
{
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);
  digitalWrite(ledPin, HIGH); //Couterintuitive, turning LED off.
  while (digitalRead(buttonPin))
  {
    //Do nothing until button is pressed, except release control of processer.
    yield();
  }
}

void loop()
{
  digitalWrite(ledPin, LOW); //Counterintuitive, turning LED on.
}